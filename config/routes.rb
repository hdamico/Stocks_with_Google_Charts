# frozen_string_literal: true

Rails.application.routes.draw do
  get 'schedule/index'

  root to: 'stocks#index'
  resources :stocks do
    post 'generate_chart', on: :collection
  end
end
