class CreateStocksTable < ActiveRecord::Migration[5.1]
  def change
    create_table :stocks do |t|
      t.string :stock_id
      t.string :chart_1
      t.string :chart_2
      t.string :chart_3
    end
  end
end