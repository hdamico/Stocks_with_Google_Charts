class Stock < ApplicationRecord
  def set_chart(stock)
    chart_url = {}
    if stock.key?(:stock_1)
      chart_url['chart_1'] = IextradingService.new.charts(stock)
    elsif stock.key?(:stock_2)
      chart_url['chart_2'] = IextradingService.new.charts(stock)
    else
      chart_url['chart_3'] = IextradingService.new.charts(stock)
    end
    update!(chart_url)
    self
  end

  def columns_map(stock)
    {
      stock_id: stock['id'],
      chart_1: stock['chart_1'],
      chart_2: stock['chart_2'],
      chart_3: stock['chart_3'],
      cuit: stock['cuit']
    }
  end
end
