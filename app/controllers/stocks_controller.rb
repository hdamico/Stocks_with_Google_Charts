# frozen_string_literal: true

class StocksController < ApplicationController
  def index
    @stock = Stock.find_or_initialize_by(id: '1')
  end

  def new
    @stock = Stock.new
  end

  def create; end

  def show; end

  def edit; end

  def update
    if @stock.update(stock_params)
      redirect_to @stock
    else
      render 'edit'
    end
  end

  def destroy; end

  def generate_chart
    @stock = Stock.find_or_initialize_by(id: '1').set_chart(params)
    render 'index'
  end
end
