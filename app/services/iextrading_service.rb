# frozen_string_literal: true

require 'net/http'
require 'uri'
require 'googlecharts'
require 'json'

class IextradingService
  BASE_URL = 'https://api.iextrading.com/1.0'

  def charts(stock)
    data = []
    url = ''

    response = api_call(url)
    response.each do |res|
      data << res['close']
    end

    generate_chart(data, stock)
  rescue StandardError
    raise ArgumentError, 'Unknown symbol' if response == 'Unknown symbol'
  end

  def generate_chart(data, stock)
    Gchart.line(data: [data],
                title: stock.upcase,
                legend: ['Valor'],
                bg: { color: 'f2f2f2' },
                bar_colors: 'ff0000,00ff00',
                axis_with_label: 'x')
  end

  def api_call(url)
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true

    request = Net::HTTP::Get.new(url)

    http.request(request)
  end
end
